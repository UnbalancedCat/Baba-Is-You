#ifndef RANK_H
#define RANK_H

#include <QMainWindow>
#include<QResizeEvent>
#include<QLabel>
#include<QFile>
#include<QDir>
#include<QMovie>
#include<QScrollBar>
#include<Block.h>
namespace Ui {
class Rank;
}

class Rank : public QMainWindow
{
    Q_OBJECT

public:
    explicit Rank(QWidget *parent = nullptr);
    ~Rank();
    int Screen=0;
    virtual void resizeEvent(QResizeEvent *event) override;
protected slots:
     void UpdateRankList();
     void TimeListChange(int);
     void NameListChange(int);
signals:
    void RankBack();

private slots:


private:
    Ui::Rank *ui;
    QLabel *BackGroundLabel = nullptr;

};



#endif // RANK_H
