 #ifndef GAMELEVEL_H
#define GAMELEVEL_H

#include <QMainWindow>
#include<QTimer>
#include<QTime>
#include<setting.h>
#include <QMessageBox>
#include<QDialog>
#include<QString>
#include<QInputDialog>
#include<QDebug>
#include <QTextStream>
#include <QMessageBox>
#include<QFile>
#include<level4.h>
#include<chooselevel.h>
#include<mainmenu.h>
#include<QMainWindow>
#include<QDir>
#include<Block.h>
#include<level1.h>
#include<QResizeEvent>
#include<level2.h>
#include<QFileDialog>
#include<QFileDevice>
#include<level3.h>
#include<level5.h>
#include<level6.h>

namespace Ui {
class GameLevel;
}

class GameLevel : public QMainWindow
{
    Q_OBJECT

public:
    explicit GameLevel(QWidget *parent = nullptr);
    ~GameLevel();
    void SendFull();
    int Screen=0;
    virtual void resizeEvent(QResizeEvent *event) override;

private slots:
    void BackMusic_slot();
    void Play_slot();
signals:
    void LevelBack();
    void FullScreen();
    void Play();
    void BackMusic_signal();
private:
    Ui::GameLevel *ui;
    int sec=0;
    int min=0;
    int hour=0;
    QLabel *BackGroundLabel = nullptr;
};

#endif // GAMELEVEL_H
