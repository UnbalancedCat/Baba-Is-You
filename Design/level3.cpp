#include "level3.h"

int preset3[20][35] = {
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,4,1,1,1,1,4,4,1,1,8,8,8,8,8,8,8,8,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,8,1,1,1,1,1,1,8,4,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,8,1,24,1,1,1,1,8,1,4,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,8,8,8,8,8,8,1,1,1,1,1,1,8,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,8,4,4,4,4,1,1,1,1,1,17,1,8,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,8,4,9,4,4,1,1,1,1,1,1,1,8,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,8,4,4,4,4,1,1,1,1,1,1,1,8,1,1,1,1,1,1,4,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,4,1,1,1,1,1,1,1,8,1,1,1,1,1,1,8,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,10,1,1,1,8,1,15,1,1,1,1,8,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,24,1,1,1,8,1,24,1,1,3,1,8,1,1,1,1,4,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,16,1,1,1,8,1,18,1,1,1,1,8,1,1,1,4,4,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,8,1,1,1,8,8,8,8,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,8,8,8,8,8,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},

};

level3::level3(QWidget *parent) : QMainWindow(parent)
{
    this->resize(1260,720);
    this->setWindowTitle("Level3");
    map.LoadMap(preset3);
}

void level3::paintEvent(QPaintEvent *){
    QString DirecR=":/image/baba_0_1.png";
    QString DirecL=":/image/baba_18_3.png";
    QString DirecU=":/image/baba_9_1.png";
    QString DirecD=":/image/baba_25_2.png";
    int width=this->width()/35;
    int height=this->height()/20;
    QPainter BackGround(this);
    BackGround.setBrush(Qt::black);
    BackGround.drawRect(this->rect());
    QPainter DrawBlock(this);
    DrawBlock.begin(this);
    int i, j;
    for(i = 0; i < 20; i++)
        for(j = 0; j < 35; j++)
            for(int k=0;k<5;k++)
              {
                  if(map.GetMap_(i,j,k)==NULL)break;
                   switch(map.GetMap_(i, j ,k)->GetBlockType())
                    {
                   case BlockType::A_BABA:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_baba_0_1.png"));break;
                   case BlockType::A_WALL:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_wall_0_1.png"));break;
                   case BlockType::A_GRASS:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_grass_0_1.png"));break;
                   case BlockType::A_ROCK:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_rock_0_1.png"));break;
                   case BlockType::A_WATER:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_water_0_1.png"));break;
                   case BlockType::A_LAVA:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_lava_0_1.png"));break;
                   case BlockType::A_FLAG:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_flag_0_1.png"));break;
                   case BlockType::A_YOU:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_you_0_1.png"));break;
                   case BlockType::A_WIN:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_win_0_1.png"));break;
                   case BlockType::A_STOP:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_stop_0_1.png"));break;
                   case BlockType::A_PUSH:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_push_0_1.png"));break;
                   case BlockType::A_SINK:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_sink_0_1.png"));break;
                   case BlockType::A_HOT:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_hot_0_1.png"));break;
                   case BlockType::A_MELT:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_melt_0_1.png"));break;
                   case BlockType::A_DEFEAT:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_defeat_0_1.png"));break;
                   case BlockType::O_BABA:
                   {
                       if(this->dirc==1)   DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(DirecR));
                       if(this->dirc==2)   DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(DirecL));
                       if(this->dirc==3)   DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(DirecU));
                       if(this->dirc==4)   DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(DirecD));
                   };break;
                   case BlockType::O_WALL:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/wall_0_1.png"));break;
                   case BlockType::O_GRASS:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/grass_0_1.png"));break;
                   case BlockType::O_ROCK:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/rock_0_1.png"));break;
                   case BlockType::O_WATER:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/water_15_1.png"));break;
                   case BlockType::O_LAVA:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/lava_15_1.png"));break;
                   case BlockType::O_FLAG:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/flag_0_1.png"));break;
                   case BlockType::G_FRONTIER:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/frontier.png"));break;
                   case BlockType::G_BLANK:break;
                   case BlockType::J_IS:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_is_0_1.png"));break;
                   case BlockType::J_AND:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/image/text_and_0_1.png"));break;
                        }
                    }
       DrawBlock.end();
}
//更新地图

void level3::keyPressEvent(QKeyEvent *ev){

    //键盘音效设置
     QMediaPlaylist *Soundmove= new QMediaPlaylist(this);
     MoveSound.setPlaylist(Soundmove);
     MoveSound.setVolume(100);
    qsrand(time(NULL));
    int n=qrand()%10;
    if(n<=6)
    {
        Soundmove->addMedia(QUrl("qrc:/music/baba_move_1.mp3"));
        MoveSound.play();
    }
    else
    {
        Soundmove->addMedia(QUrl("qrc:/music/baba_move_2.mp3"));
        MoveSound.play();
    }


    if(ev->key() == Qt::Key_Up){
        dirc=3;
        MoveDirection up= MoveDirection::UP;
        map.InitBlockAttribution();
        map.JudgeBlockAttribution();
        map.GetYouCoordinate();
        map.Move(up);
        if(map.GetYouCoordinate()==false){
            QString dlgTitle="Game Over!";
            QString strInfo="Unfortunately,you died!";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::information(this, dlgTitle, strInfo,QMessageBox::Ok,defaultBtn);
            if(result==QMessageBox::Ok)
            emit this->Back();
            map.LoadMap(preset3);
        }
        if(map.JudgeWin()==true&&this->mode==0){
            QString dlgTitle="Congradulation!";
            QString strInfo="Level3 Finished!Do you want to continue?";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
            if (result==QMessageBox::Yes)
                    emit this->NextLevel();
                else if(result==QMessageBox::No)
                    emit this->Back();
            map.LoadMap(preset3);
        }
        if(map.JudgeWin()==true&&this->mode==1){
            emit this->NextLevel();
            map.LoadMap(preset3);
        }
        update();

    }
    if(ev->key() == Qt::Key_Down){
        dirc=4;
        MoveDirection down= MoveDirection::DOWN;
        map.InitBlockAttribution();
        map.JudgeBlockAttribution();
        map.GetYouCoordinate();
        map.Move(down);
        if(map.GetYouCoordinate()==false){
            QString dlgTitle="Game Over!";
            QString strInfo="Unfortunately,you died!";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::information(this, dlgTitle, strInfo,QMessageBox::Ok,defaultBtn);
            if(result==QMessageBox::Ok)
            emit this->Back();
            map.LoadMap(preset3);
        }
        if(map.JudgeWin()==true&&this->mode==0){
            QString dlgTitle="Congradulation!";
            QString strInfo="Level3 Finished!Do you want to continue?";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
            if (result==QMessageBox::Yes)
                    emit this->NextLevel();
                else if(result==QMessageBox::No)
                    emit this->Back();
            map.LoadMap(preset3);
        }

        if(map.JudgeWin()==true&&this->mode==1){
            emit this->NextLevel();
            map.LoadMap(preset3);
        }
        update();

    }
    if(ev->key() == Qt::Key_Left){
        dirc=2;
        MoveDirection left= MoveDirection::LEFT;
        map.InitBlockAttribution();
        map.JudgeBlockAttribution();
        map.GetYouCoordinate();
        map.Move(left);
        if(map.GetYouCoordinate()==false){
            QString dlgTitle="Game Over!";
            QString strInfo="Unfortunately,you died!";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::information(this, dlgTitle, strInfo,QMessageBox::Ok,defaultBtn);
            if(result==QMessageBox::Ok)
            emit this->Back();
            map.LoadMap(preset3);
        }
        if(map.JudgeWin()==true&&this->mode==0){
            QString dlgTitle="Congradulation!";
            QString strInfo="Level3 Finished!Do you want to continue?";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
            if (result==QMessageBox::Yes)
                    emit this->NextLevel();
                else if(result==QMessageBox::No)
                    emit this->Back();
            map.LoadMap(preset3);
        }

        if(map.JudgeWin()==true&&this->mode==1){
            emit this->NextLevel();
            map.LoadMap(preset3);
        }
        update();

    }
    if(ev->key() == Qt::Key_Right){
        dirc=1;
        MoveDirection right= MoveDirection::RIGHT;
        map.InitBlockAttribution();
        map.JudgeBlockAttribution();
        map.GetYouCoordinate();
        map.Move(right);
        if(map.GetYouCoordinate()==false){
            QString dlgTitle="Game Over!";
            QString strInfo="Unfortunately,you died!";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::information(this, dlgTitle, strInfo,QMessageBox::Ok,defaultBtn);
            if(result==QMessageBox::Ok)
            emit this->Back();
            map.LoadMap(preset3);

        }
        if(map.JudgeWin()==true&&this->mode==0){
            QString dlgTitle="Congradulation!";
            QString strInfo="Level3 Finished!Do you want to continue?";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
            if (result==QMessageBox::Yes)
                    emit this->NextLevel();
                else if(result==QMessageBox::No)
                    emit this->Back();
            map.LoadMap(preset3);
        }

        if(map.JudgeWin()==true&&this->mode==1){
            emit this->NextLevel();
            map.LoadMap(preset3);
        }
        update();
    }
    if(ev->key()==Qt::Key_Escape){
        QString dlgTitle="Quit";
        QString strInfo="Do you want to quit?";
        QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
        QMessageBox::StandardButton result;//返回选择的按钮
        result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
        if(result==QMessageBox::Yes){
            emit this->Back();
            map.LoadMap(preset3);
        }
    }

}

void level3::resizeEvent(QResizeEvent *event){
    update();
}
