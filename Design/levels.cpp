#include "levels.h"

Levels::Levels(QWidget *parent) : QMainWindow(parent)
{
    this->resize(1260,720);
    this->setWindowTitle("Chalenge Yourself");
    map1.LoadMap(preset1);
    level2 *Level2 =new level2(this);
    level3 *Level3 =new level3(this);
}

void levels::paintEvent(QPaintEvent *){
    int width=this->width()/35;
    int height=this->height()/20;
    QPainter BackGround(this);
    BackGround.setBrush(Qt::black);
    BackGround.drawRect(this->rect());
    QPainter DrawBlock(this);
    DrawBlock.begin(this);
    int i, j;
    for(i = 0; i < 20; i++)
        for(j = 0; j < 35; j++)
            for(int k=0;k<5;k++)
              {
                  if(map.GetMap_(i,j,k)==NULL)break;
                   switch(map.GetMap_(i, j ,k)->GetBlockType())
                    {
                        case BlockType::A_BABA:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_baba_0_1.png"));break;
                        case BlockType::A_WALL:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_wall_0_1.png"));break;
                        case BlockType::A_GRASS:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_greass_0_1.png"));break;
                        case BlockType::A_ROCK:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_rock_0_1.png"));break;
                        case BlockType::A_WATER:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_water_0_1.png"));break;
                        case BlockType::A_LAVA:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_lava_0_1.png"));break;
                        case BlockType::A_FLAG:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_flag_0_1.png"));break;
                        case BlockType::A_YOU:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_you_0_1.png"));break;
                        case BlockType::A_WIN:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_win_0_1.png"));break;
                        case BlockType::A_STOP:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_stop_0_1.png"));break;
                        case BlockType::A_PUSH:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_push_0_1.png"));break;
                        case BlockType::A_SINK:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_sink_0_1.png"));break;
                        case BlockType::A_HOT:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_hot_0_1.png"));break;
                        case BlockType::A_MELT:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_melt_0_1.png"));break;
                        case BlockType::A_DEFEAT:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_defeat_0_1.png"));break;
                        case BlockType::O_BABA:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/baba_0_1.png"));break;
                        case BlockType::O_WALL:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/wall_0_1.png"));break;
                        case BlockType::O_GRASS:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/grass_0_1.png"));break;
                        case BlockType::O_ROCK:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/rock_0_1.png"));break;
                        case BlockType::O_WATER:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/water_15_1.png"));break;
                        case BlockType::O_LAVA:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/water_15_1.png"));break;
                        case BlockType::O_FLAG:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/flag_0_1.png"));break;
                        case BlockType::G_FRONTIER:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/frontier.png"));break;
                        case BlockType::G_BLANK:break;
                        case BlockType::J_IS:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_is_0_1.png"));break;
                        case BlockType::J_AND:DrawBlock.drawPixmap(width * j, height * i, width, height, QPixmap(":/text_and_0_1.png"));break;
                        }
                    }
       DrawBlock.end();
}


void levels::keyPressEvent(QKeyEvent *ev){


    if(ev->key() == Qt::Key_Up){
        MoveDirection up= MoveDirection::UP;
        map.InitBlockAttribution();
        map.JudgeBlockAttribution();
        map.GetYouCoordinate();
        map.Move(up);
        if(map.GetYouCoordinate()==false){
            QString dlgTitle="Game Over!";
            QString strInfo="Unfortunately,You died!";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::information(this, dlgTitle, strInfo,QMessageBox::Ok,defaultBtn);
            if(result==QMessageBox::Ok)
            emit this->Back();
        }
        if(map.JudgeWin()==true){
            QString dlgTitle="Congradulation!";
            QString strInfo="Level1 Finished!Do you want to continue?";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
            if (result==QMessageBox::Yes)
                    emit this->NextLevel();
                else if(result==QMessageBox::No)
                    emit this->Back();
        }
        update();

    }
    if(ev->key() == Qt::Key_Down){
        MoveDirection down= MoveDirection::DOWN;
        map.InitBlockAttribution();
        map.JudgeBlockAttribution();
        map.GetYouCoordinate();
        map.Move(down);
        if(map.GetYouCoordinate()==false){
            QString dlgTitle="Game Over!";
            QString strInfo="Unfortunately,You died!";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::information(this, dlgTitle, strInfo,QMessageBox::Ok,defaultBtn);
            if(result==QMessageBox::Ok)
            emit this->Back();
            map.LoadMap(preset1);
        }
        if(map.JudgeWin()==true){
            QString dlgTitle="Congradulation!";
            QString strInfo="Level1 Finished!Do you want to continue?";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
            if (result==QMessageBox::Yes)
                    emit this->NextLevel();
                else if(result==QMessageBox::No)
                    emit this->Back();
            map.LoadMap(preset1);
        }
        update();

    }
    if(ev->key() == Qt::Key_Left){
        MoveDirection left= MoveDirection::LEFT;
        map.InitBlockAttribution();
        map.JudgeBlockAttribution();
        map.GetYouCoordinate();
        map.Move(left);
        if(map.GetYouCoordinate()==false){
            QString dlgTitle="Game Over!";
            QString strInfo="Unfortunately,You died!";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::information(this, dlgTitle, strInfo,QMessageBox::Ok,defaultBtn);
            if(result==QMessageBox::Ok)
            emit this->Back();
            map.LoadMap(preset1);
        }
        if(map.JudgeWin()==true){
            QString dlgTitle="Congradulation!";
            QString strInfo="Level1 Finished!Do you want to continue?";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
            if (result==QMessageBox::Yes)
                    emit this->NextLevel();
                else if(result==QMessageBox::No)
                    emit this->Back();
            map.LoadMap(preset1);
        }
        update();

    }
    if(ev->key() == Qt::Key_Right){
        MoveDirection right= MoveDirection::RIGHT;
        map.InitBlockAttribution();
        map.JudgeBlockAttribution();
        map.GetYouCoordinate();
        map.Move(right);
        if(map.GetYouCoordinate()==false){
            QString dlgTitle="Game Over!";
            QString strInfo="Unfortunately,You died!";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::information(this, dlgTitle, strInfo,QMessageBox::Ok,defaultBtn);
            if(result==QMessageBox::Ok)
            emit this->Back();
            map.LoadMap(preset1);

        }
        if(map.JudgeWin()==true){
            QString dlgTitle="Congradulation!";
            QString strInfo="Level1 Finished!Do you want to continue?";
            QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
            QMessageBox::StandardButton result;//返回选择的按钮
            result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
            if (result==QMessageBox::Yes)
                    emit this->NextLevel();
                else if(result==QMessageBox::No)
                    emit this->Back();
            map.LoadMap(preset1);
        }
        update();
     }
    if(ev->key()==Qt::Key_Escape){
        QString dlgTitle="Quit";
        QString strInfo="Do you want to quit?";
        QMessageBox::StandardButton  defaultBtn=QMessageBox::NoButton; //缺省按钮
        QMessageBox::StandardButton result;//返回选择的按钮
        result=QMessageBox::question(this, dlgTitle, strInfo,QMessageBox::Yes|QMessageBox::No,defaultBtn);
        if(result==QMessageBox::Yes){
            emit this->Back();
            map.LoadMap(preset1);
        }
    }




}

void levels::resizeEvent(QResizeEvent *event){
    update();
}


