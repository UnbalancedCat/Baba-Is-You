#include <cstddef>
#include <iostream>
#include <vector>
#include "Block.h"
//类函数的实现

//基类 块
Block::Block(BlockType bolocktype) :blocktype_(bolocktype), overlappable_(false) {}
void Block::InitBlockType(BlockType blocktype, bool overlappable)
{
    blocktype_ = blocktype;
    overlappable_ = overlappable;
}
void Block::InitAttribution(bool overlappable)
{
    overlappable_ = overlappable;
}
bool Block::GetAttribution(void)
{
    return overlappable_;
}
BlockType Block::GetBlockType(void)
{
    return blocktype_;
}


//派生类 实物块
Object::Object(BlockType blocktype)//实物块根据判断决定是否为实体，是否可重叠，是否可推动，是否被 hot 消除，但一定可被 sink 以及 defeat 消除
{
    Block::InitBlockType(blocktype, true);
    you_ = win_ = stop_ = push_ = sink_ = hot_ = melt_ = defeat_ = false;//需等待判断，因此初始化所有属性
}
void Object::InitAttribution(bool boollist[9])
{
    Block::InitAttribution(boollist[0]);
    you_ = boollist[1];
    win_ = boollist[2];
    stop_ = boollist[3];
    push_ = boollist[4];
    sink_ = boollist[5];
    hot_ = boollist[6];
    melt_ = boollist[7];
    defeat_ = boollist[8];
}
bool Object::GetAttribution(bool boollist[9])
{
    boollist[0] = Block::GetAttribution();
    boollist[1] = you_;
    boollist[2] = win_;
    boollist[3] = stop_;
    boollist[4] = push_;
    boollist[5] = sink_;
    boollist[6] = hot_;
    boollist[7] = melt_;
    boollist[8] = defeat_;
    return Block::GetAttribution();
}

//派生类 属性块
Attribution::Attribution(BlockType blocktype)//属性块必须为实体，不可重叠，可推动，可被 hot、sink 以及 defeat 消除
{
    Block::InitBlockType(blocktype, false);
}

//派生类 判断块
Judgement::Judgement(BlockType blocktype)//属性块必须为实体，不可重叠，可推动，可被 hot、sink 以及 defeat 消除
{
    Block::InitBlockType(blocktype, false);
}

//类 游戏布局
Map::Map()
{
    num_you_ = 0;
    int i, j;
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < 35; j++)
        {
            map_[i][j][0] = new Block();
        }
    }
}
void Map::LoadMap(int preset[20][35])
{
    int i, j;
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < 35; j++)
        {
            switch (preset[i][j])
            {
            case 0:map_[i][j][0]->InitBlockType(BlockType::G_FRONTIER, false); break;
            case 1:map_[i][j][0]->InitBlockType(BlockType::G_BLANK, true); break;
            case 2:map_[i][j][0] = new Object(BlockType::O_BABA); break;
            case 3:map_[i][j][0] = new Object(BlockType::O_WALL); break;
            case 4:map_[i][j][0] = new Object(BlockType::O_GRASS); break;
            case 5:map_[i][j][0] = new Object(BlockType::O_ROCK); break;
            case 6:map_[i][j][0] = new Object(BlockType::O_WATER); break;
            case 7:map_[i][j][0] = new Object(BlockType::O_LAVA); break;
            case 8:map_[i][j][0] = new Object(BlockType::O_FLAG); break;
            case 9:map_[i][j][0] = new Attribution(BlockType::A_BABA); break;
            case 10:map_[i][j][0] = new Attribution(BlockType::A_WALL); break;
            case 11:map_[i][j][0] = new Attribution(BlockType::A_GRASS); break;
            case 12:map_[i][j][0] = new Attribution(BlockType::A_ROCK); break;
            case 13:map_[i][j][0] = new Attribution(BlockType::A_WATER); break;
            case 14:map_[i][j][0] = new Attribution(BlockType::A_LAVA); break;
            case 15:map_[i][j][0] = new Attribution(BlockType::A_FLAG); break;
            case 16:map_[i][j][0] = new Attribution(BlockType::A_YOU); break;
            case 17:map_[i][j][0] = new Attribution(BlockType::A_WIN); break;
            case 18:map_[i][j][0] = new Attribution(BlockType::A_STOP); break;
            case 19:map_[i][j][0] = new Attribution(BlockType::A_PUSH); break;
            case 20:map_[i][j][0] = new Attribution(BlockType::A_SINK); break;
            case 21:map_[i][j][0] = new Attribution(BlockType::A_HOT); break;
            case 22:map_[i][j][0] = new Attribution(BlockType::A_MELT); break;
            case 23:map_[i][j][0] = new Attribution(BlockType::A_DEFEAT); break;
            case 24:map_[i][j][0] = new Judgement(BlockType::J_IS); break;
            case 25:map_[i][j][0] = new Judgement(BlockType::J_AND); break;
            default:std::cout << "Fail to load the mappreset!" << std::endl;return;
            }
        }
    }
    int n;
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < 35; j++)
        {
            for(n = 1; n < 99; n++)
            {
                map_[i][j][n] = NULL;
            }
        }
    }
}
void Map::InitBlockAttribution(void)
{
    bool boollist[9] = { false,false,false,false,false,false,false,false,false };
    int i, j, n;
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < 35; j++)
        {
            n = 0;
            while (true)
            {
                if (map_[i][j][n] == NULL)break;
                if (map_[i][j][n]->GetBlockType() >= BlockType::O_BABA && map_[i][j][n]->GetBlockType() <= BlockType::O_FLAG)
                {
                    map_[i][j][n]->InitAttribution(boollist);
                }
                n++;
            }
        }
    }
}
void Map::JudgeBlockAttribution(void)
{
    int i, j, n;
    std::vector<int> x;
    std::vector<int> y;
    //定位 IS
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < 35; j++)
        {
            if (map_[i][j][0]->GetBlockType() == BlockType::J_IS)
            {
                x.push_back(i);
                y.push_back(j);
            }
        }
    }
    int max = x.size();
    int k;
    //纵向判断
    for (k = 0; k < max; k++)
    {
        if (
            map_[x[k] - 1][y[k]][0]->GetBlockType() >= BlockType::A_BABA &&
            map_[x[k] - 1][y[k]][0]->GetBlockType() <= BlockType::A_FLAG &&
            map_[x[k] + 1][y[k]][0]->GetBlockType() >= BlockType::A_YOU &&
            map_[x[k] + 1][y[k]][0]->GetBlockType() <= BlockType::A_DEFEAT
            )
        {
            switch (map_[x[k] - 1][y[k]][0]->GetBlockType())
            {
            case(BlockType::A_BABA):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_BABA)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k] + 1][y[k]][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_WALL):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_WALL)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k] + 1][y[k]][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_GRASS):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_GRASS)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k] + 1][y[k]][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_ROCK):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_ROCK)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k] + 1][y[k]][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_WATER):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_WATER)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k] + 1][y[k]][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_LAVA):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_LAVA)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k] + 1][y[k]][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_FLAG):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_FLAG)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k] + 1][y[k]][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            }
        }
    }
    //横向判断
    for (k = 0; k < max; k++)
    {
        if (
            map_[x[k]][y[k] - 1][0]->GetBlockType() >= BlockType::A_BABA &&
            map_[x[k]][y[k] - 1][0]->GetBlockType() <= BlockType::A_FLAG &&
            map_[x[k]][y[k] + 1][0]->GetBlockType() >= BlockType::A_YOU &&
            map_[x[k]][y[k] + 1][0]->GetBlockType() <= BlockType::A_DEFEAT
            )
        {
            switch (map_[x[k]][y[k] - 1][0]->GetBlockType())
            {
            case(BlockType::A_BABA):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_BABA)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k]][y[k] + 1][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_WALL):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_WALL)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k]][y[k] + 1][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_GRASS):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_GRASS)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k]][y[k] + 1][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_ROCK):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_ROCK)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k]][y[k] + 1][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_WATER):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_WATER)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k]][y[k] + 1][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_LAVA):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_LAVA)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k]][y[k] + 1][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            case(BlockType::A_FLAG):
                for (i = 0; i < 20; i++)
                {
                    for (j = 0; j < 35; j++)
                    {
                        n = 0;
                        while (true)
                        {
                            if (map_[i][j][n] == NULL)break;
                            if (map_[i][j][n]->GetBlockType() == BlockType::O_FLAG)
                            {
                                bool boollist[9];
                                map_[i][j][n]->GetAttribution(boollist);
                                switch (map_[x[k]][y[k] + 1][0]->GetBlockType())
                                {
                                case BlockType::A_YOU:
                                    boollist[1] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_WIN:
                                    boollist[2] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_STOP:
                                    boollist[3] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_PUSH:
                                    boollist[4] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_SINK:
                                    boollist[5] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_HOT:
                                    boollist[6] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_MELT:
                                    boollist[7] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                case BlockType::A_DEFEAT:
                                    boollist[8] = true;
                                    map_[i][j][n]->InitAttribution(boollist);
                                    break;
                                }
                            }
                            n++;
                        }
                    }
                }
                break;
            }
        }
    }
}
bool Map::GetYouCoordinate(void)
{
    x_.clear();
    y_.clear();
    n_.clear();
    num_you_ = 0;
    int i, j, n;
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < 35; j++)
        {
            if (map_[i][j][0]->GetBlockType() >= BlockType::O_BABA && map_[i][j][0]->GetBlockType() <= BlockType::O_FLAG)
            {
                n = 0;
                while (true)
                {
                    if (map_[i][j][n] == NULL)break;
                    bool boollist[9];
                    map_[i][j][n]->GetAttribution(boollist);
                    if (boollist[1] == true)
                    {
                        x_.push_back(i);
                        y_.push_back(j);
                        n_.push_back(n);
                        num_you_++;
                    }
                    n++;
                }
            }
        }
    }
    if (num_you_ == 0)return false;
    else return true;
}
bool Map::JudgeWin(void)
{
    int k, n;
    for (k = 0; k < num_you_; k++)
    {
        n = 0;
        while (true)
        {
            if (map_[x_[k]][y_[k]][n] == NULL)break;
            bool boollist[9];
            map_[x_[k]][y_[k]][n]->GetAttribution(boollist);
            if (boollist[2] == true)
            {
                return true;
            }
            else n++;
        }
    }
    return false;
}
bool Map::JudgeMove(int x, int y, int n, MoveDirection movedirection)
{
    bool receive;
    switch (movedirection)
    {
    case MoveDirection::UP:
    {
        if (map_[x - 1][y][0]->GetBlockType() == BlockType::G_FRONTIER)//移动方向是边界，直接跳出
            return false;
        else if (map_[x - 1][y][0]->GetBlockType() == BlockType::G_BLANK)//移动方向是空白，直接交换
        {
            map_[x - 1][y][0] = map_[x][y][n];
            if (n != 0)map_[x][y][n] = NULL;
            else map_[x][y][n] = new Block(BlockType::G_BLANK);
            return true;
        }
        else if ((map_[x - 1][y][0]->GetBlockType() >= BlockType::A_BABA && map_[x - 1][y][0]->GetBlockType() <= BlockType::A_DEFEAT) || (map_[x - 1][y][0]->GetBlockType() == BlockType::J_IS || map_[x - 1][y][0]->GetBlockType() == BlockType::J_AND))//移动方向是属性块（必能推动,进行连推判断）
        {
            receive = JudgeMove(x - 1, y, 0, movedirection);
            if (receive == true)
            {
                map_[x - 1][y][0] = map_[x][y][n];
                if (n != 0)map_[x][y][n] = NULL;
                else map_[x][y][n] = new Block(BlockType::G_BLANK);
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (map_[x - 1][y][0]->GetBlockType() >= BlockType::O_BABA && map_[x - 1][y][0]->GetBlockType() <= BlockType::O_FLAG)//移动方向是实物块（判断属性, 有叠层）
        {
            int m;
            for (m = 0;; m++)
            {
                if (map_[x - 1][y][m] == NULL)
                                {
                                    break;
                                }
                if (map_[x - 1][y][m]->GetBlockType() <= BlockType::J_AND && map_[x - 1][y][m]->GetBlockType() >= BlockType::A_BABA)
                                {
                                    receive = JudgeMove(x - 1, y, m, movedirection);
                                    if (receive == true)
                                    {
                                        if (map_[x - 1][y][0] == map_[x][y][n])
                                        {
                                            if (n != 0)map_[x][y][n] = NULL;
                                            else map_[x][y][n] = new Block(BlockType::G_BLANK);
                                        }
                                        else
                                        {
                                            map_[x - 1][y][m] = map_[x][y][n];
                                            if (n != 0)map_[x][y][n] = NULL;
                                            else map_[x][y][n] = new Block(BlockType::G_BLANK);
                                        }
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                bool boollist_you[9];
                bool boollist_destination[9];
                map_[x][y][n]->GetAttribution(boollist_you);
                map_[x - 1][y][m]->GetAttribution(boollist_destination);
                //高优先度
                //push推动
                if (boollist_destination[4] == true)
                {
                    receive = JudgeMove(x - 1, y, 0, movedirection);
                    if (receive == true)
                    {
                        map_[x - 1][y][0] = map_[x][y][n];
                        if (n != 0)map_[x][y][n] = NULL;
                        else map_[x][y][n] = new Block(BlockType::G_BLANK);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else {
                    //stop停止
                    if (boollist_destination[3] == true)
                    {
                        return false;
                    }
                    else
                    {
                        //低优先度
                        //sink两个都消失
                        if (boollist_destination[5] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            if (m != 0)map_[x - 1][y][m] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                        //melt配对消失
                        if (boollist_you[7] == true && boollist_destination[6] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                        //defeat使消失
                        if (boollist_destination[8] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                    }
                }
            }
            map_[x - 1][y][m] = map_[x][y][n];
            if (n != 0)map_[x][y][n] = NULL;
            else map_[x][y][n] = new Object(BlockType::G_BLANK);
        }
        break;
    }
    case MoveDirection::DOWN:
    {
        if (map_[x + 1][y][0]->GetBlockType() == BlockType::G_FRONTIER)//移动方向是边界，直接跳出
            return false;
        else if (map_[x + 1][y][0]->GetBlockType() == BlockType::G_BLANK)//移动方向是空白，直接交换
        {
            map_[x + 1][y][0] = map_[x][y][n];
            if (n != 0)map_[x][y][n] = NULL;
            else map_[x][y][n] = new Block(BlockType::G_BLANK);
            return true;
        }
        else if ((map_[x + 1][y][0]->GetBlockType() >= BlockType::A_BABA && map_[x + 1][y][0]->GetBlockType() <= BlockType::A_DEFEAT) || (map_[x + 1][y][0]->GetBlockType() == BlockType::J_IS || map_[x + 1][y][0]->GetBlockType() == BlockType::J_AND))//移动方向是属性块（必能推动,进行连推判断）
        {
            receive = JudgeMove(x + 1, y, 0, movedirection);
            if (receive == true)
            {
                map_[x + 1][y][0] = map_[x][y][n];
                if (n != 0)map_[x][y][n] = NULL;
                else map_[x][y][n] = new Block(BlockType::G_BLANK);
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (map_[x + 1][y][0]->GetBlockType() >= BlockType::O_BABA && map_[x + 1][y][0]->GetBlockType() <= BlockType::O_FLAG)//移动方向是实物块（判断属性, 有叠层）
        {
            int m;
            for (m = 0;; m++)
            {
                if (map_[x + 1][y][m] == NULL)
                                {
                                    break;
                                }
                                if (map_[x + 1][y][m]->GetBlockType() <= BlockType::J_AND && map_[x + 1][y][m]->GetBlockType() >= BlockType::A_BABA)
                                {
                                    receive = JudgeMove(x + 1, y, m, movedirection);
                                    if (receive == true)
                                    {
                                        if (map_[x + 1][y][0] == map_[x][y][n])
                                        {
                                            if (n != 0)map_[x][y][n] = NULL;
                                            else map_[x][y][n] = new Block(BlockType::G_BLANK);
                                        }
                                        else
                                        {
                                            map_[x + 1][y][m] = map_[x][y][n];
                                            if (n != 0)map_[x][y][n] = NULL;
                                            else map_[x][y][n] = new Block(BlockType::G_BLANK);
                                        }
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                bool boollist_you[9];
                bool boollist_destination[9];
                map_[x][y][n]->GetAttribution(boollist_you);
                map_[x + 1][y][m]->GetAttribution(boollist_destination);
                //高优先度
                //push推动
                if (boollist_destination[4] == true)
                {
                    receive = JudgeMove(x + 1, y, 0, movedirection);
                    if (receive == true)
                    {
                        map_[x + 1][y][0] = map_[x][y][n];
                        if (n != 0)map_[x][y][n] = NULL;
                        else map_[x][y][n] = new Block(BlockType::G_BLANK);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else {
                    //stop停止
                    if (boollist_destination[3] == true)
                    {
                        return false;
                    }
                    else
                    {
                        //低优先度
                        //sink两个都消失
                        if (boollist_destination[5] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            if (m != 0)map_[x + 1][y][m] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                        //melt配对消失
                        if (boollist_you[7] == true && boollist_destination[6] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                        //defeat使消失
                        if (boollist_destination[8] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                    }
                }
            }
            map_[x + 1][y][m] = map_[x][y][n];
            if (n != 0)map_[x][y][n] = NULL;
            else map_[x][y][n] = new Object(BlockType::G_BLANK);
        }
        break;
    }
    case MoveDirection::LEFT:
    {
        if (map_[x][y - 1][0]->GetBlockType() == BlockType::G_FRONTIER)//移动方向是边界，直接跳出
            return false;
        else if (map_[x][y - 1][0]->GetBlockType() == BlockType::G_BLANK)//移动方向是空白，直接交换
        {
            map_[x][y - 1][0] = map_[x][y][n];
            if (n != 0)map_[x][y][n] = NULL;
            else map_[x][y][n] = new Block(BlockType::G_BLANK);
            return true;
        }
        else if ((map_[x][y - 1][0]->GetBlockType() >= BlockType::A_BABA && map_[x][y - 1][0]->GetBlockType() <= BlockType::A_DEFEAT) || (map_[x][y - 1][0]->GetBlockType() == BlockType::J_IS || map_[x][y - 1][0]->GetBlockType() == BlockType::J_AND))//移动方向是属性块（必能推动,进行连推判断）
        {
            receive = JudgeMove(x, y - 1, 0, movedirection);
            if (receive == true)
            {
                map_[x][y - 1][0] = map_[x][y][n];
                if (n != 0)map_[x][y][n] = NULL;
                else map_[x][y][n] = new Block(BlockType::G_BLANK);
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (map_[x][y - 1][0]->GetBlockType() >= BlockType::O_BABA && map_[x][y - 1][0]->GetBlockType() <= BlockType::O_FLAG)//移动方向是实物块（判断属性, 有叠层）
        {
            int m;
            for (m = 0;; m++)
            {
                if (map_[x][y - 1][m] == NULL)//问题
                                {
                                    break;
                                }
                                if (map_[x][y - 1][m]->GetBlockType() <= BlockType::J_AND && map_[x][y - 1][m]->GetBlockType() >= BlockType::A_BABA)
                                {
                                    receive = JudgeMove(x, y - 1, m, movedirection);
                                    if (receive == true)
                                    {
                                        if (map_[x][y - 1][0] == map_[x][y][n])
                                        {
                                            if (n != 0)map_[x][y][n] = NULL;
                                            else map_[x][y][n] = new Block(BlockType::G_BLANK);
                                        }
                                        else
                                        {
                                            map_[x][y - 1][m] = map_[x][y][n];
                                            if (n != 0)map_[x][y][n] = NULL;
                                            else map_[x][y][n] = new Block(BlockType::G_BLANK);
                                        }

                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                bool boollist_you[9];
                bool boollist_destination[9];
                map_[x][y][n]->GetAttribution(boollist_you);
                map_[x][y - 1][m]->GetAttribution(boollist_destination);
                //高优先度
                //push推动
                if (boollist_destination[4] == true)
                {
                    receive = JudgeMove(x, y - 1, 0, movedirection);
                    if (receive == true)
                    {
                        map_[x][y - 1][0] = map_[x][y][n];
                        if (n != 0)map_[x][y][n] = NULL;
                        else map_[x][y][n] = new Block(BlockType::G_BLANK);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else {
                    //stop停止
                    if (boollist_destination[3] == true)
                    {
                        return false;
                    }
                    else
                    {
                        //低优先度
                        //sink两个都消失
                        if (boollist_destination[5] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            if (m != 0)map_[x][y - 1][m] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                        //melt配对消失
                        if (boollist_you[7] == true && boollist_destination[6] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                        //defeat使消失
                        if (boollist_destination[8] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                    }
                }
            }
            map_[x][y - 1][m] = map_[x][y][n];
            if (n != 0)map_[x][y][n] = NULL;
            else map_[x][y][n] = new Object(BlockType::G_BLANK);
        }
        break;
    }
    case MoveDirection::RIGHT:
    {
        if (map_[x][y + 1][0]->GetBlockType() == BlockType::G_FRONTIER)//移动方向是边界，直接跳出
            return false;
        else if (map_[x][y + 1][0]->GetBlockType() == BlockType::G_BLANK)//移动方向是空白，直接交换
        {
            map_[x][y + 1][0] = map_[x][y][n];
            if (n != 0)map_[x][y][n] = NULL;
            else map_[x][y][n] = new Block(BlockType::G_BLANK);
            return true;
        }
        else if ((map_[x][y + 1][0]->GetBlockType() >= BlockType::A_BABA && map_[x][y + 1][0]->GetBlockType() <= BlockType::A_DEFEAT) || (map_[x][y + 1][0]->GetBlockType() == BlockType::J_IS || map_[x][y + 1][0]->GetBlockType() == BlockType::J_AND))//移动方向是属性块（必能推动,进行连推判断）
        {
            receive = JudgeMove(x, y + 1, 0, movedirection);
            if (receive == true)
            {
                map_[x][y + 1][0] = map_[x][y][n];
                if (n != 0)map_[x][y][n] = NULL;
                else map_[x][y][n] = new Block(BlockType::G_BLANK);
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (map_[x][y + 1][0]->GetBlockType() >= BlockType::O_BABA && map_[x][y + 1][0]->GetBlockType() <= BlockType::O_FLAG)//移动方向是实物块（判断属性, 有叠层）
        {
            int m;
            for (m = 0;; m++)
            {
                if (map_[x][y + 1][m] == NULL)
                                {
                                    break;
                                }
                                if (map_[x][y + 1][m]->GetBlockType() <= BlockType::J_AND && map_[x][y + 1][m]->GetBlockType() >= BlockType::A_BABA)
                                {
                                    receive = JudgeMove(x, y + 1, m, movedirection);
                                    if (receive == true)
                                    {
                                        if (map_[x][y + 1][0] == map_[x][y][n])
                                        {
                                            if (n != 0)map_[x][y][n] = NULL;
                                            else map_[x][y][n] = new Block(BlockType::G_BLANK);
                                        }
                                        else
                                        {
                                            map_[x][y + 1][m] = map_[x][y][n];
                                            if (n != 0)map_[x][y][n] = NULL;
                                            else map_[x][y][n] = new Block(BlockType::G_BLANK);
                                        }
                                        return true;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                bool boollist_you[9];
                bool boollist_destination[9];
                map_[x][y][n]->GetAttribution(boollist_you);
                map_[x][y + 1][m]->GetAttribution(boollist_destination);
                //高优先度
                //push推动
                if (boollist_destination[4] == true)
                {
                    receive = JudgeMove(x, y + 1, 0, movedirection);
                    if (receive == true)
                    {
                        map_[x][y + 1][0] = map_[x][y][n];
                        if (n != 0)map_[x][y][n] = NULL;
                        else map_[x][y][n] = new Block(BlockType::G_BLANK);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else {
                    //stop停止
                    if (boollist_destination[3] == true)
                    {
                        return false;
                    }
                    else
                    {
                        //低优先度
                        //sink两个都消失
                        if (boollist_destination[5] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            if (m != 0)map_[x][y + 1][m] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                        //melt配对消失
                        if (boollist_you[7] == true && boollist_destination[6] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                        //defeat使消失
                        if (boollist_destination[8] == true)
                        {
                            if (n != 0)map_[x][y][n] = NULL;
                            else map_[x][y][n] = new Object(BlockType::G_BLANK);
                            return true;
                        }
                    }
                }
            }
            map_[x][y + 1][m] = map_[x][y][n];
            if (n != 0)map_[x][y][n] = NULL;
            else map_[x][y][n] = new Object(BlockType::G_BLANK);
        }
        break;
    }
    }
    return true;
}
void Map::Move(MoveDirection movedirection)
{
    int k;
    for (k = 0; k < num_you_; k++)
    {
        JudgeMove(x_[k], y_[k], n_[k], movedirection);
    }
}
Block* Map::GetMap_(int x, int y,int n)
{
    return map_[x][y][n];
}
