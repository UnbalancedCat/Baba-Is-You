#ifndef GAMEONETIME_H
#define GAMEONETIME_H

#include <QWidget>

namespace Ui {
class GameOneTime;
}

class GameOneTime : public QWidget
{
    Q_OBJECT

public:
    explicit GameOneTime(QWidget *parent = nullptr);
    ~GameOneTime();

private:
    Ui::GameOneTime *ui;
};

#endif // GAMEONETIME_H
