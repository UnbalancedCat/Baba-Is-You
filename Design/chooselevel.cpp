#include "chooselevel.h"
#include "ui_chooselevel.h"


ChooseLevel::ChooseLevel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChooseLevel)
{
    BackGroundLabel = new QLabel(this);
    BackGroundLabel->resize(840,480);
    BackGroundLabel->setScaledContents(true);
    QMovie *movie = new QMovie(":/image/setting.gif");
    BackGroundLabel->setMovie(movie);
    movie->start();
                    //设置动图背景


    ui->setupUi(this);
    this->resize(1260,720);
    this->setWindowTitle("ChooseLevel");

    level1 *Level1=new level1(this);
    level2 *Level2=new level2(this);
    level3 *Level3=new level3(this);
    level4 *Level4=new level4(this);
    level5 *Level5=new level5(this);
    level6 *Level6=new level6(this);


    connect(ui->level1,&QPushButton::clicked,[=](){
        emit this->ChangeMusic();
        Level1->mode=0;
        this->hide();
        if(this->Screen==0)
        Level1->showNormal();
        else {
            Level1->showFullScreen();
        }

    });
    connect(ui->level2,&QPushButton::clicked,[=](){
        emit this->ChangeMusic();
        Level2->mode=0;
        this->hide();
        if(this->Screen==0)
        Level2->showNormal();
        else {
            Level2->showFullScreen();
        }

    });
    connect(ui->level3,&QPushButton::clicked,[=](){
        emit this->ChangeMusic();
        Level3->mode=0;
        this->hide();
        if(this->Screen==0)
        Level3->showNormal();
        else {
            Level3->showFullScreen();
        }

    });
    connect(ui->level4,&QPushButton::clicked,[=](){
        emit this->ChangeMusic();
        Level4->mode=0;
        this->hide();
        if(this->Screen==0)
        Level4->showNormal();
        else {
            Level4->showFullScreen();
        }

    });
    connect(ui->level5,&QPushButton::clicked,[=](){
        emit this->ChangeMusic();
        Level5->mode=0;
        this->hide();
        if(this->Screen==0)
        Level5->showNormal();
        else {
            Level5->showFullScreen();
        }

    });
    connect(ui->level6,&QPushButton::clicked,[=](){
        emit this->ChangeMusic();
        Level6->mode=0;
        this->hide();
        if(this->Screen==0)
        Level6->showNormal();
        else {
            Level6->showFullScreen();
        }

    });
    //进入某关


    connect(Level1,&level1::NextLevel,[=](){
        Level1->close();
        Level2->show();
    });
    connect(Level1,&level1::Back,[=](){
        emit this->BackMusic();
        Level1->close();
        this->show();
    });
    connect(Level2,&level2::NextLevel,[=](){
        Level2->close();
        Level3->show();
    });
    connect(Level2,&level2::Back,[=](){
        emit this->BackMusic();
        Level2->close();
        this->show();
    });
    connect(Level3,&level3::NextLevel,[=](){
        Level3->close();
        Level4->show();
    });
    connect(Level3,&level3::Back,[=](){
        emit this->BackMusic();
        Level3->close();
        this->show();
    });
    connect(Level4,&level4::NextLevel,[=](){
        Level4->close();
        Level5->show();
    });
    connect(Level4,&level4::Back,[=](){
        emit this->BackMusic();
        Level4->close();
        this->show();
    });
    connect(Level5,&level5::NextLevel,[=](){
        Level5->close();
        Level6->show();
    });
    connect(Level5,&level5::Back,[=](){
        emit this->BackMusic();
        Level5->close();
        this->show();
    });

    connect(Level6,&level6::Back,[=](){
        emit this->BackMusic();
        Level6->close();
        this->show();
    });

//下一关与返回上一级

    connect(ui->ChooseLevelBack,&QPushButton::clicked,[=](){
        emit this->ChooseLevelBack();
    });
}

ChooseLevel::~ChooseLevel()
{
    delete ui;
}

void ChooseLevel::resizeEvent(QResizeEvent *event){
    BackGroundLabel->resize(this->size());
}
//重载窗口大小变化事件，使背景随窗口大小变化
