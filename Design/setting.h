#ifndef SETTING_H
#define SETTING_H

#include <QMainWindow>
#include<QMediaPlayer>
#include<QLabel>
#include<QResizeEvent>
#include<QSlider>
#include<QMediaPlayer>
#include<QMediaPlaylist>
#include<QDir>
#include<QMovie>
#include<Block.h>
#include<QMediaContent>

namespace Ui {
class Setting;
}

class Setting : public QMainWindow
{
    Q_OBJECT

public:
    explicit Setting(QWidget *parent = nullptr);
    ~Setting();
    int Screen=0;
    virtual void resizeEvent(QResizeEvent *event) override;
signals:
    void SettingBack();
    void FullScreen();
    void QuitFullScreen();

private slots:
    void on_horizontalSlider_sliderMoved(int position);//调节音量槽函数
    void ChangeMusic();
    void BackMusic();
private:
    Ui::Setting *ui;
    QMediaPlayer GameSound;//播放器
    QMediaPlaylist *SoundList=nullptr;
    QLabel *BackGroundLabel = nullptr;
};

#endif // SETTING_H
