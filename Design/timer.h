#ifndef TIMER_H
#define TIMER_H

#include <QObject>
#include<QDialog>
#include<QTime>
#include<QTimer>
class Dialog : public QDialog
{
private:
    QTimer *fTimer; //定时器
    QTime fTimeCounter;//计时器
private slots:
    void on_timer_timeout () ; //定时溢出处理槽函数
};

#endif // TIMER_H
