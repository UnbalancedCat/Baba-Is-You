#pragma once
#include <cstddef>
#include <iostream>
#include <vector>

#ifndef BLOCK_H_
#define BLOCK_H_

//定义枚举类型
enum class MoveDirection
{
    UP,
    DOWN,
    LEFT,
    RIGHT,
};
enum class BlockType
{
    //Frontier 边界 Blank 空白
    G_FRONTIER,
    G_BLANK,

    //Object 实物块
    O_BABA,
    O_WALL,
    O_GRASS,
    O_ROCK,
    O_WATER,
    O_LAVA,
    O_FLAG,

    //Attribution 属性块
    A_BABA,
    A_WALL,
    A_GRASS,
    A_ROCK,
    A_WATER,
    A_LAVA,
    A_FLAG,
 //OVERLAPPABLE	//0是否可重叠
    A_YOU,		//1自身属性：指定控制对象
    A_WIN,		//2对外属性：YOU 重叠时过关
    A_STOP,		//3 高优先度 对外属性：阻止其他对象穿过，不能被重叠
    A_PUSH,		//4 高优先度 对外属性：移动一个方格远离进入他的物体，不能被重叠
    A_SINK,		//5对外属性：被任意对象重叠时，破坏对象及本身
    A_HOT,		//6对外属性：与有 MELT 属性的对象重叠时，破坏对象及本身
    A_MELT,		//7自身属性：仅含有与 HOT 的属性
    A_DEFEAT,	//8对外属性：破坏任何重叠的物体

    //Judgement 判断块
    J_IS,
    J_AND,
};

//类的声明
class Block//基类 块
{
private:
    BlockType blocktype_;
    bool overlappable_;	//固有属性 可重叠性
public:
    Block(BlockType bolocktype = BlockType::G_FRONTIER);
    void InitBlockType(BlockType blocktype,bool overlappable);
    void InitAttribution(bool overlappable);
    virtual void InitAttribution(bool boollist[9]) {};
    bool GetAttribution(void);
    virtual bool GetAttribution(bool boollist[9]) { return overlappable_; };
    BlockType GetBlockType(void);
};
class Object : public Block//派生类 实物块
{
private:
    bool you_, win_, stop_, push_, sink_, hot_, melt_, defeat_;
public:
    Object(BlockType blocktype);
    void InitAttribution(bool boollist[9]);
    bool GetAttribution(bool boollist[9]);
};
class Attribution : public Block//派生类 属性块
{
private:

public:
    Attribution(BlockType blocktype);
};
class Judgement : public Block//派生类 判断块
{
public:
    Judgement(BlockType blocktype);
};

class Map //游戏布局
{
private:
    Block* map_[20][35][99];	//约为9:16
    std::vector<int> x_;
    std::vector<int> y_;
    std::vector<int> n_;	//记录所有有 YOU 属性的 块 的坐标
    int num_you_;			//记录 有 YOU 属性的 块 的个数
public:
    Map();
    void LoadMap(int preset[20][35]);
    void InitBlockAttribution(void);
    void JudgeBlockAttribution(void);
    bool GetYouCoordinate(void);
    bool JudgeWin(void);
    bool JudgeMove(int x, int y, int n, MoveDirection movedirection);
    void Move(MoveDirection movedirection);
    Block* GetMap_(int x, int y,int n);

};
#endif
