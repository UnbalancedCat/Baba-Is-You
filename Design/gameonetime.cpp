#include "gameonetime.h"
#include "ui_gameonetime.h"

GameOneTime::GameOneTime(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GameOneTime)
{
    ui->setupUi(this);
}

GameOneTime::~GameOneTime()
{
    delete ui;
}
