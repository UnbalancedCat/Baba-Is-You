#ifndef CHOOSELEVEL_H
#define CHOOSELEVEL_H

#include <QWidget>
#include<Block.h>
#include<QResizeEvent>
#include<QLabel>
#include<QMovie>
#include<level4.h>
#include<level1.h>
#include<level2.h>
#include<level3.h>
#include<level5.h>
#include<level6.h>

namespace Ui {
class ChooseLevel;
}

class ChooseLevel : public QWidget
{
    Q_OBJECT

public:
    explicit ChooseLevel(QWidget *parent = nullptr);
    ~ChooseLevel();
    virtual void resizeEvent(QResizeEvent *event) override;
    int Screen=0;
signals:
    void ChooseLevelBack();
    void ChangeMusic();
    void BackMusic();
private:
    Ui::ChooseLevel *ui;
    QLabel *BackGroundLabel = nullptr;

};

#endif // CHOOSELEVEL_H
