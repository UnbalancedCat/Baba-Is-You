#ifndef LEVELS_H
#define LEVELS_H

#include <QMainWindow>
#include<QPainter>
#include<QPaintEvent>
#include<Block.h>
#include<QKeyEvent>
#include<QResizeEvent>
#include<QDebug>
#include<QMessageBox>
#include<level1.h>
#include<level2.h>
#include<level3.h>

class Levels : public QMainWindow
{
    Q_OBJECT
public:
    explicit Levels(QWidget *parent = nullptr);
protected:
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *ev);
    void resizeEvent(QResizeEvent *event);
private:
   Map map1;
signals:
   void NextLevel1();
   void Back1();
};

#endif // LEVELS_H
