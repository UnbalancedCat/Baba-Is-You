#include "setting.h"
#include "ui_setting.h"


Setting::Setting(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Setting)
{
    BackGroundLabel = new QLabel(this);
    BackGroundLabel->resize(1260,720);
    BackGroundLabel->setScaledContents(true);
    QMovie *movie = new QMovie(":/image/setting.gif");
    BackGroundLabel->setMovie(movie);
    movie->start();//设置背景
    ui->setupUi(this);
    setWindowTitle("Setting");
    connect(ui->SettingBack,&QPushButton::clicked,[=](){
      emit this->SettingBack();
    });
    connect(ui->FullScreen,&QPushButton::clicked,[=](){
        if(Screen==0)
        {
            emit this->FullScreen();
            Screen=1;
        }
        else if(Screen==1)
        {
            emit this->QuitFullScreen();
            Screen=0;
        }
    });
      SoundList = new QMediaPlaylist(this);//创建播放列表
//     QString runPath = QDir::currentPath();
//     QDir dir(runPath);
//     dir.cdUp( );
//     QString AP = dir.absolutePath() + "/Design/music/Menu.mp3";//将路径从程序执行路径转为源文件所在路径下的music路径
//     AP.replace(QString("/"), QString("\\"));//创建绝对路径
     SoundList->addMedia(QUrl("qrc:/music/Menu.mp3"));
     SoundList->setCurrentIndex(0);
     SoundList->addMedia(QUrl("qrc:/music/level.mp3"));
     SoundList->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);//循环播放
     GameSound.setPlaylist(SoundList);
     GameSound.setVolume(100);
     GameSound.play();
     ui->horizontalSlider->setValue(100);
     connect(parent,SIGNAL(Play()),this,SLOT(ChangeMusic()));//切换到关卡音乐
     connect(parent,SIGNAL(BackMusic_signal()),this,SLOT(BackMusic()));//切换回主菜单音乐
}

Setting::~Setting()
{
    delete ui;
}



void Setting::on_horizontalSlider_sliderMoved(int position)
{
    GameSound.setVolume(position);
}

void Setting::resizeEvent(QResizeEvent *event){
    BackGroundLabel->resize(this->size());
}
//重载窗口大小变化事件，使背景随窗口大小变化

void Setting::ChangeMusic(){
    SoundList->setCurrentIndex(1);
}

void Setting::BackMusic(){
    SoundList->setCurrentIndex(0);
}
