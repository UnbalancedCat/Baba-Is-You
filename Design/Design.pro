QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Block.cpp \
    chooselevel.cpp \
    gamelevel.cpp \
    level1.cpp \
    level2.cpp \
    level3.cpp \
    level4.cpp \
    level5.cpp \
    level6.cpp \
    main.cpp \
    mainmenu.cpp \
    rank.cpp \
    setting.cpp

HEADERS += \
    Block.h \
    chooselevel.h \
    gamelevel.h \
    level1.h \
    level2.h \
    level3.h \
    level4.h \
    level5.h \
    level6.h \
    mainmenu.h \
    rank.h \
    setting.h

FORMS += \
    chooselevel.ui \
    gamelevel.ui \
    mainmenu.ui \
    rank.ui \
    setting.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc

RC_FILE = myapp.rc


