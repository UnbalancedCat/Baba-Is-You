#include "gamelevel.h"
#include "ui_gamelevel.h"

GameLevel::GameLevel(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GameLevel)
{

    BackGroundLabel = new QLabel(this);
    BackGroundLabel->resize(1260,720);
    BackGroundLabel->setScaledContents(true);
    QMovie *movie = new QMovie(":/image/setting.gif");
    BackGroundLabel->setMovie(movie);
    movie->start();
     ui->setupUi(this);
    setWindowTitle("GameLevel");
    ChooseLevel *Chooselevel =new ChooseLevel;
    connect(ui->ChooseLevel,&QPushButton::clicked,[=](){
        this->hide();
        if(this->Screen==0)
            Chooselevel->showNormal();
        else {
                Chooselevel->showFullScreen();
                Chooselevel->Screen=1;
        }
    });

    level1 *Level1=new level1(this);
    level2 *Level2=new level2(this);
    level3 *Level3=new level3(this);
    level4 *Level4=new level4(this);
    level5 *Level5=new level5(this);
    level6 *Level6=new level6(this);



    connect(ui->GameLevelBack,&QPushButton::clicked,[=](){
      emit this->LevelBack();
    });

    QTimer *timer= new QTimer(this);
    connect(ui->GameOneTime,&QPushButton::clicked,[=](){
         timer->start(1000);
    });//开始一关到底模式的计时

    connect(timer,&QTimer::timeout,[=](){
        sec++;
    });
    connect(Level6,&level6::StopTime,[=](){
        timer->stop();
        min=(sec-sec%60)/60;
        sec=sec%60;
        hour=(min-min%60)/60;
        min=min%60;
        QString TotalTime;
        if(hour<10){
            TotalTime=QString::asprintf("Finish Time:0%d:",hour);
        }
        else TotalTime=QString::asprintf("Finish Time:%d:",hour);
        if(min<10){
            TotalTime+=QString::asprintf("0%d:",min);
        }
        else TotalTime+=QString::asprintf("%d:",min);
        if(sec<10){
            TotalTime+=QString::asprintf("0%d",sec);
        }
        else TotalTime+=QString::asprintf("%d",sec);

        QMessageBox con(this);
        con.setWindowTitle("Congradulation!");
        con.setText(TotalTime);
        con.exec();
        bool ok;
        QString name =QInputDialog::getText(this,tr("Input Your Name"),tr("Your Name:"),QLineEdit::Normal,tr(""),&ok);
        if(ok){
//            QString PathOfName = QDir::currentPath();
//            QDir dir(PathOfName);
//            dir.cdUp( );
//            QString name1 = dir.absolutePath() + "/Design/data/RankRecord_name.txt";
            QFile Name("RankRecord_name.txt");

            Name.open(QIODevice::Append);
            QTextStream stream(&Name);
            stream<<name<<endl;


            Name.close();
//            QString PathOfTime = QDir::currentPath();
//            QDir dir1(PathOfTime);
//            dir1.cdUp( );
//            QString time = dir1.absolutePath() + "/Design/data/RankRecord_time.txt";
            QFile Time("RankRecord_time.txt");
            Time.open(QIODevice::Append);
            QTextStream stream1(&Time);
            if(hour<10){
                stream1<<"0"<<hour<<":";
            }
            else stream1<<hour<<":";
            if(min<10){
                stream1<<"0"<<min<<":";
            }
            else stream1<<min<<":";
            if(sec<10){
                stream1<<"0"<<sec<<endl;
            }
            else stream1<<sec<<endl;
            Time.close();
            hour=sec=min=0;
        }
    });//结束计时

    connect(ui->GameOneTime,&QPushButton::clicked,[=](){
        emit this->Play();
        Level1->mode=1;
        this->hide();
        if(this->Screen==0)
            Level1->showNormal();
        else Level1->showFullScreen();
    });


    connect(Level1,&level1::NextLevel,[=](){
        Level1->close();
        Level2->mode=1;
        if(this->Screen==0)
            Level2->showNormal();
        else Level2->showFullScreen();
    });
    connect(Level2,&level2::NextLevel,[=](){
        Level2->close();
        Level3->mode=1;
        if(this->Screen==0)
            Level3->showNormal();
        else Level3->showFullScreen();
    });
    connect(Level3,&level3::NextLevel,[=](){
        Level3->close();
        Level4->mode=1;
        if(this->Screen==0)
            Level4->showNormal();
        else Level4->showFullScreen();
    });
    connect(Level4,&level4::NextLevel,[=](){
        Level4->close();
        Level5->mode=1;
        if(this->Screen==0)
            Level5->showNormal();
        else Level5->showFullScreen();
    });
    connect(Level5,&level5::NextLevel,[=](){
        Level5->close();
        Level6->mode=1;
        if(this->Screen==0)
            Level6->showNormal();
        else Level6->showFullScreen();
    });
    connect(Level6,&level6::StopTime,[=](){
        Level6->close();
        this->show();
    });
        //一关到底跳转到下一关与返回


    connect(Level1,&level1::Back,[=](){
        emit this->BackMusic_signal();
        Level1->close();
        this->show();
    });
    connect(Level2,&level2::Back,[=](){
        emit this->BackMusic_signal();
        Level2->close();
        this->show();
    });
    connect(Level3,&level3::Back,[=](){
        emit this->BackMusic_signal();
        Level3->close();
        this->show();
    });
    connect(Level4,&level4::Back,[=](){
        emit this->BackMusic_signal();
        Level4->close();
        this->show();
    });
    connect(Level5,&level5::Back,[=](){
        Level5->close();
        this->show();
    });
    connect(Level6,&level6::Back,[=](){
        emit this->BackMusic_signal();
        Level6->close();
        this->show();
    });
        //一关到底返回菜单

    connect(Chooselevel,&ChooseLevel::ChooseLevelBack,[=](){
        Chooselevel->close();
        this->show();
    });
    connect(Chooselevel,SIGNAL(ChangeMusic()),this,SLOT(Play_slot()));
    connect(Chooselevel,SIGNAL(BackMusic()),this,SLOT(BackMusic_slot()));
}

GameLevel::~GameLevel()
{
    delete ui;
}

void GameLevel::SendFull(){
    emit this->FullScreen();
}

void GameLevel::resizeEvent(QResizeEvent *event){
    BackGroundLabel->resize(this->size());
}
//重载窗口大小变化事件，使背景随窗口大小变化

void GameLevel::BackMusic_slot(){
    emit this->BackMusic_signal();
}

void GameLevel::Play_slot(){
    emit this->Play();
}
