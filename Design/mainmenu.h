#ifndef MAINMENU_H
#define MAINMENU_H

#include <QMainWindow>
#include<QLabel>
#include<gamelevel.h>
#include<setting.h>
#include <QtGui>
#include<QPushButton>
#include<QMediaPlayer>
#include<QMediaPlaylist>
#include<QResizeEvent>
#include<Block.h>
QT_BEGIN_NAMESPACE
namespace Ui { class MainMenu; }
QT_END_NAMESPACE

class MainMenu : public QMainWindow
{
    Q_OBJECT

public:
    MainMenu(QWidget *parent = nullptr);
    ~MainMenu();
    int Screen=0;
    virtual void resizeEvent(QResizeEvent *event);
private slots:
    void ChangeMusic();
    void BackMusic_slot();
signals:
    void UpdateRank();
    void Play();
    void BackMusic_signal();
private:
    Ui::MainMenu *ui;
    QLabel *BackGroundLabel = nullptr;
};
#endif // MAINMENU_H
