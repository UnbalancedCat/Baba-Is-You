#include "mainmenu.h"
#include "ui_mainmenu.h"
#include<rank.h>

MainMenu::MainMenu(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainMenu)
{
    BackGroundLabel = new QLabel(this);
    BackGroundLabel->resize(1260,720);
    BackGroundLabel->setScaledContents(true);
    QMovie *movie = new QMovie(":/image/menu.gif");
    BackGroundLabel->setMovie(movie);
    movie->start();

    ui->setupUi(this);
    setWindowTitle("BaBa Is You");
    this->resize(1260,720);
    connect(ui->QuitBtn,&QPushButton::clicked,[=](){
        this->close();
    });
    Rank *RankMenu =new Rank(this);
    GameLevel *GameLevelMenu =new GameLevel(this);
    Setting *SetMenu=new Setting(this);
    RankMenu->resize(1260,720);
    GameLevelMenu->resize(1260,720);
    SetMenu->resize(1260,720);
    ui->StartBtn->setStyleSheet("QPushButton{border-image: url(:/image/start.png);}"
                               "QPushButton:hover{border-image: url(:/image/start_hover.png);}"
                               "QPushButton:pressed{border-image: url(:/image/start_pressed.png);}");
    ui->RankBtn->setStyleSheet("QPushButton{border-image: url(:/image/rank.png);}"
                               "QPushButton:hover{border-image: url(:/image/rank_hover.png);}"
                               "QPushButton:pressed{border-image: url(:/image/rank_pressed.png);}");
    ui->SetBtn->setStyleSheet("QPushButton{border-image: url(:/image/setting.png);}"
                               "QPushButton:hover{border-image: url(:/image/setting_hover.png);}"
                               "QPushButton:pressed{border-image: url(:/image/setting_pressed.png);}");
    ui->QuitBtn->setStyleSheet("QPushButton{border-image: url(:/image/quit.png);}"
                               "QPushButton:hover{border-image: url(:/image/quit_hover.png);}"
                               "QPushButton:pressed{border-image: url(:/image/quit_pressed.png);}");

    connect(ui->RankBtn,&QPushButton::clicked,[=](){
        emit this->UpdateRank();
        this->hide();
        if(RankMenu->Screen==1){
            RankMenu->showFullScreen();
        }
        else RankMenu->showNormal();

    });
    connect(ui->SetBtn,&QPushButton::clicked,[=](){
        this->hide();
        if(SetMenu->Screen==1){
            SetMenu->showFullScreen();
        }
        else SetMenu->showNormal();
    });
    connect(ui->StartBtn,&QPushButton::clicked,[=](){
        this->hide();
        if(GameLevelMenu->Screen==1){
            GameLevelMenu->showFullScreen();
        }
        else GameLevelMenu->showNormal();
    });
    //子窗口跳转

    connect(GameLevelMenu,&GameLevel::LevelBack,[=](){
        GameLevelMenu->hide();
        if(this->Screen==1){
            this->showFullScreen();
        }
        else this->showNormal();
     });
    connect(SetMenu,&Setting::SettingBack,[=](){
        SetMenu->hide();
        if(this->Screen==1){
            this->showFullScreen();
        }
        else this->showNormal();
     });
    connect(RankMenu,&Rank::RankBack,[=](){
        RankMenu->hide();
        if(this->Screen==1){
            this->showFullScreen();
        }
        else this->showNormal();
     });
    //子窗口返回

    connect(SetMenu,&Setting::FullScreen,[=](){
        SetMenu->showFullScreen();
        SetMenu->Screen=1;
        RankMenu->Screen=1;
        GameLevelMenu->Screen=1;
        this->Screen=1;
    });
    connect(SetMenu,&Setting::QuitFullScreen,[=](){
        SetMenu->showNormal();
        SetMenu->Screen=0;
        GameLevelMenu->Screen=0;
        RankMenu->Screen=0;
        this->Screen=0;
    });
    //设置全屏

    connect(GameLevelMenu,SIGNAL(Play()),this,SLOT(ChangeMusic()));//切换到关卡音乐
    connect(GameLevelMenu,SIGNAL(BackMusic_signal()),this,SLOT(BackMusic_slot()));//切换回主菜单音乐

}

MainMenu::~MainMenu()
{
    delete ui;
}

void MainMenu::resizeEvent(QResizeEvent *event){
    BackGroundLabel->resize(this->size());
}
//重载窗口大小变化事件，使背景随窗口大小变化

void MainMenu::ChangeMusic(){
    emit this->Play();
}

void MainMenu::BackMusic_slot(){
    emit this->BackMusic_signal();
}
