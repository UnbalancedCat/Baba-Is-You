#include "rank.h"
#include "ui_rank.h"
#include<mainmenu.h>

Rank::Rank(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Rank)
{
    BackGroundLabel = new QLabel(this);
    BackGroundLabel->resize(1260,720);
    BackGroundLabel->setScaledContents(true);
    QMovie *movie = new QMovie(":/image/ranklist.gif");
    BackGroundLabel->setMovie(movie);
    movie->start();//设置背景
    ui->setupUi(this);
    setWindowTitle("Rank");
    connect(ui->RankBack,&QPushButton::clicked,[=](){
      emit this->RankBack();
    });
    connect(parent,SIGNAL(UpdateRank()),this,SLOT(UpdateRankList()));
    ui->NameList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->TimeList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    QScrollBar *name =ui->NameList->verticalScrollBar();
    QScrollBar *time =ui->TimeList->verticalScrollBar();
    connect(name,SIGNAL(valueChanged(int)),this,SLOT(TimeListChange(int)));
    connect(time,SIGNAL(valueChanged(int)),this,SLOT(NameListChange(int)));

}

Rank::~Rank()
{
    delete ui;
}

void Rank::UpdateRankList(){
    ui->NameList->clear();
    ui->TimeList->clear();
    QString PathOfName = QDir::currentPath();
    QDir dir(PathOfName);
    dir.cdUp( );
    //QString name = dir.absolutePath() + "/Design/data/RankRecord_name.txt";
    QFile Name("RankRecord_name.txt");
    Name.open(QIODevice::ReadOnly);
    QByteArray NameLine;
    while(!Name.atEnd()){
        NameLine=Name.readLine();
        ui->NameList->addItem(NameLine);
    }

    QString PathOfTime = QDir::currentPath();
    QDir dir1(PathOfTime);
    dir1.cdUp( );
    //QString time = dir1.absolutePath() + "/Design/data/RankRecord_time.txt";
    QFile Time("RankRecord_time.txt");
    Time.open(QIODevice::ReadOnly);
    QByteArray TimeLine;
    while(!Time.atEnd()){
        TimeLine=Time.readLine();
        ui->TimeList->addItem(TimeLine);
    }
} //实时更新排行榜

void Rank::resizeEvent(QResizeEvent *event){
    BackGroundLabel->resize(this->size());
}
//重载窗口大小变化事件，使背景随窗口大小变化





void Rank::TimeListChange(int a){
    ui->TimeList->verticalScrollBar()->setValue(a);
}

void Rank::NameListChange(int a){
    ui->NameList->verticalScrollBar()->setValue(a);
}
