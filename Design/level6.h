#ifndef LEVEL6_H
#define LEVEL6_H

#include <QMainWindow>
#include<QPainter>
#include<QPaintEvent>
#include<Block.h>
#include<QKeyEvent>
#include<QResizeEvent>
#include<QDebug>
#include<QMessageBox>
#include<QtGlobal>
#include<ctime>
#include<setting.h>

class level6 : public QMainWindow
{
    Q_OBJECT
public:
    explicit level6(QWidget *parent = nullptr);
    int mode=0;//0代表从选关模式调用，1代表从一关到底调用
protected:
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *ev);
    void resizeEvent(QResizeEvent *event);

private:
    Map map;
    int dirc=1;//1右，2左，3上，4下
    QMediaPlayer MoveSound;//移动音效播放器
signals:
    void Back();
    void StopTime();
};


#endif // LEVEL6_H
